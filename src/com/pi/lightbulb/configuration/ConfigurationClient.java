package com.pi.lightbulb.configuration;

import java.util.ArrayList;
import java.util.Scanner;

import org.ws4d.java.CoreFramework;
import org.ws4d.java.authorization.AuthorizationException;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.DPWSProtocolInfo;
import org.ws4d.java.communication.DPWSProtocolVersion;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.dispatch.DefaultServiceReference;
import org.ws4d.java.dispatch.DeviceServiceRegistry;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.HelloData;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.SearchParameter;
import org.ws4d.java.types.URI;
import org.ws4d.java.types.XAddressInfo;
import org.ws4d.java.types.XAddressInfoSet;
import org.ws4d.java.util.Log;


/**
 * Configuration client
 */
public class ConfigurationClient extends DefaultClient implements ConfigurationServiceInterface {

	private ArrayList<DPWSInterface> dpwsInterfaces;
	DPWSRule rule;
	private Thread ruleExecution;
	static boolean stopThread = false;

	/**
	 * air conditioner device object
	 */
	Device airConditioner;

	/**
	 * operation object for the increase operation
	 */
	Operation increaseValueOp;

	public ConfigurationClient() {
		dpwsInterfaces = new ArrayList<DPWSInterface>();
	}

	@Override
	public ArrayList<DPWSInterface> getSupportedInterfaces() {
		return dpwsInterfaces;
	}

	public void setSupportedInterfaces(ArrayList<DPWSInterface> list) {
		dpwsInterfaces = list;
	}

	@Override
	public void connectService(Endpoint endpoint) {
		//		EndpointReference epr = new EndpointReference(new AttributedURI("urn:uuid:4e125b40-6632-11e2-80d5-fcbdecddf1fb"));
//		XAddressInfo xAddress = new XAddressInfo(new URI("http://139.2.61.217:63597/4e293ea0-6632-11e2-80d8-fcbdecddf1fb"));
//		xAddress.setProtocolInfo(new DPWSProtocolInfo(DPWSProtocolVersion.DPWS_VERSION_2009));
//		XAddressInfoSet addresses = new XAddressInfoSet(xAddress);

		DeviceReference defRef = DeviceServiceRegistry.getDeviceReference(new EndpointReference(endpoint.address), 
				SecurityKey.EMPTY_KEY, CommunicationManagerRegistry.getPreferredCommunicationManagerID(), true);
		try {
			Device dev = defRef.getDevice();
			System.out.println("Device: " + dev);
			DefaultServiceReference serviceReference = (DefaultServiceReference) dev.getServiceReference(endpoint.serviceId, SecurityKey.EMPTY_KEY);
			System.out.println("Service: " + serviceReference);
			// we know the searched service's id
//			DefaultServiceReference sRef = (DefaultServiceReference) dev.getServiceReference(DocuExampleService.DOCU_EXAMPLE_SERVICE_ID, SecurityKey.EMPTY_KEY);
//			Service serv = sRef.getService();
			
			ruleExecution = new Thread(new ruleExe(this, dev, serviceReference));

//			// we are looking for an Operation by its name
//			Operation op = serv.getOperation(null, "DocuExampleSimpleOperation", null, null);
//
//			ParameterValue input = op.createInputValue();
//			ParameterValueManagement.setString(input, DocuExampleSimpleOperation.NAME, "Number One");
//
//			ParameterValue result;
//			try {
//				result = op.invoke(input, CredentialInfo.EMPTY_CREDENTIAL_INFO);
//				System.out.println("Response from the DocuExampleSimpleOperation: " + result.toString());
//			} catch (AuthorizationException e) {
//				e.printStackTrace();
//			} catch (InvocationException e) {
//				e.printStackTrace();
//			}
		} catch (CommunicationException e) {
			e.printStackTrace();
		}

	}

	@Override
	public ArrayList<DPWSService> getConnectedServices() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void disconnectService(DPWSService service) {
		// TODO Auto-generated method stub

	}

	@Override
	public ArrayList<DPWSParameterDescription> getRuleParameters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<DPWSRule> getRules() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRule(DPWSRule rule) {
		// TODO Auto-generated method stub

		this.rule = rule;
		ruleExecution.start();
		
	}


};
