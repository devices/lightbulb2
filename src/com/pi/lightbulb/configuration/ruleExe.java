package com.pi.lightbulb.configuration;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.dispatch.DefaultServiceReference;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;

import com.pi.lightbulb.configuration.ConfigurationServiceInterface.DPWSRule;

public class ruleExe implements Runnable {

	ConfigurationClient cl;
	Device dev;
	DefaultServiceReference serv;
	
	public ruleExe(ConfigurationClient clExt, Device dev, DefaultServiceReference serv){
		cl = clExt;
		this.dev = dev;
		this.serv = serv;
		
	}
	@Override
	public void run() {
		DPWSRule rule = cl.rule;
		try {
			Operation op = serv.getService().getOperation(new QName("Brightness", "http://www.demo.com/bbsr"), "LightOperation", null, null);
			//name of operation is known from wsdl
			
			while(true){
			
			ParameterValue result = op.invoke(null, null);											//read luminosity value from sensor
//			
			int light = Integer.parseInt(ParameterValueManagement.getString(result, "light"));		//value of the sensor
			int i = Integer.parseInt(rule.conditions.get(0).value);									//value to compare with
			
			switch(rule.conditions.get(0).operator)													//comparison operator
			{
			case Equal:
				if(light == i)
					System.out.println("Light ON");
				else
					System.out.println("Light OFF");
				break;
			case Greater:
				if(light > i)
				System.out.println("Light ON");
				else
					System.out.println("Light OFF");
				break;
			case GreaterOrEqual:
				if(light >= i)
				System.out.println("Light ON");
				else
					System.out.println("Light OFF");
				break;
			case Less:
				if(light < i)
				System.out.println("Light ON");
				else
					System.out.println("Light OFF");
				break;
			case LessOrQqual:
				if(light <= i)
				System.out.println("Light ON");
				else
					System.out.println("Light OFF");
				break;
			case NotEqual:
				if(light != i)
				System.out.println("Light ON");
				else
					System.out.println("Light OFF");
				break;
			}
			
			Thread.sleep(1000);
			}	
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
