package com.pi.lightbulb.configuration;

import org.ws4d.java.types.URI;

public class Constants {

	public final static String	NAMESPACE_CONFIGURATION		= "http://www.ws4d.org/Configuration";
	
	public final static String	NAMESPACE_DEMO				= "http://www.demo.com/bbsr";
	
	public final static String	NAMESPACE					= "http://www.demo.com/bbsr";
	
	public final static String	DEVICE						= "LightBulb";
	
	public final static String	WSDL						= "local:/com/pi/lightbulb/device/Switch.wsdl";
	
	public final static String	CONFIGURATION_WSDL			= "local:/com/pi/lightbulb/configuration/Configuration.wsdl";
	
	public final static String	SERVICE_ID					= "LightBulbService";
	
	public final static String	CONFIGURATION_SERVICE_ID	= "ConfigurationService";
	
	public final static String 	SERVICE_TYPE 				= "Switch";
	
	public final boolean ON_DEVICE							= false;

}
