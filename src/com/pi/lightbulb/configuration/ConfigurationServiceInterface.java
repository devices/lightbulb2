package com.pi.lightbulb.configuration;

import java.util.ArrayList;

import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.URI;

public interface ConfigurationServiceInterface {
	
	public enum Role {Service, Client};
	public enum DataType {Integer, UnsignedInteger, Time, Date, Boolean, Float, String};
	public enum Operator {Equal, NotEqual, Greater, GreaterOrEqual, Less, LessOrQqual};
	public enum LogicOperation {AND, OR, NAND, NOR, XOR, XNOR};
	
	public class DPWSInterface {
//		public QName type;
		public String localName;
		public URI namespace;
		public Role role;
		public boolean ruleSupport;
	}
	
	public class Endpoint {
		public URI address;
		public DPWSInterface device;
		public DPWSInterface portType;
		public URI serviceId;
	}
	
	public class DPWSService {				
		public Endpoint endpoint;
		public int role;
		public int id;
	}
	
	public class DPWSParameterDescription {
		public String name;
		public DataType type;
	}
	
	public class DPWSRuleParameter {
		public String name;
		public String value;
	}
	
	public class DPWSCondition {
		public String parameter;
		public Operator operator;
		public String value;
	}
	
	public class DPWSRule {
		public class Action {
			public String operation;
			public ArrayList<DPWSRuleParameter> parameters = new ArrayList<DPWSRuleParameter>();
		}
		
		public ArrayList<DPWSCondition> conditions = new ArrayList<DPWSCondition>();
		public LogicOperation connection;
		public Action action = new Action();
		public String name;
		public int id;
	}
	
	public ArrayList<DPWSInterface> getSupportedInterfaces();
	
	public void connectService(Endpoint endpoint);
	
	public ArrayList<DPWSService> getConnectedServices();
	
	public void disconnectService(DPWSService service);
	
	public ArrayList<DPWSParameterDescription> getRuleParameters();
	
	public ArrayList<DPWSRule> getRules();
	
	public void setRule(DPWSRule rule);

}

