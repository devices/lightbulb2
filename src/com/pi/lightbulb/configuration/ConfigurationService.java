package com.pi.lightbulb.configuration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.service.EventSourceStub;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.InvokeDelegate;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.OperationStub;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.EndpointReference2004;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.URI;

import com.pi.lightbulb.configuration.ConfigurationServiceInterface.*;

public class ConfigurationService extends DefaultService {
	
	private ConfigurationClient client;
	private ArrayList<DPWSParameterDescription> parameterDescriptionList = new ArrayList<DPWSParameterDescription>();
	private HashMap<Integer, DPWSRule> rulesList1 = new HashMap<Integer, DPWSRule>();
	private HashMap<Integer, Endpoint> connectedServices = new HashMap<Integer, Endpoint>();
	
	public ConfigurationService() {
		try {
			define(new URI(Constants.CONFIGURATION_WSDL), CredentialInfo.EMPTY_CREDENTIAL_INFO, CommunicationManagerRegistry.getPreferredCommunicationManagerID());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setServiceId(new URI(Constants.CONFIGURATION_SERVICE_ID));
		
		client = new ConfigurationClient();
		
		configureGetSupportedInterfacesOperation();
		configureConnectServiceOperation();
		configureGetRulesOperation();
		configureGetRuleParametersOperation();
		configureSetRuleOperation();
		configureDeleteRuleOperation();
		configureGetConnectedServicesOperation();
		configureDisconnectServiceOperation();
	}
	
	public void setConfigurationClient(ConfigurationClient client) {
		this.client = client;
	}
	
	public ConfigurationClient getConfigurationClient() {
		return client;
	}
	
	private void configureGetSupportedInterfacesOperation()
	{
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) getOperation(null, "GetSupportedInterfaces", null, null);

		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invokeImpl(Operation operation, ParameterValue request, CredentialInfo credentialInfo) throws InvocationException {
				// extract the expected input args ...
				//String value = ParameterValueManagement.getString(request, "someValue");

				// create suitable response ...
				ParameterValue result = operation.createOutputValue();
				
				ArrayList<DPWSInterface> interfaces = client.getSupportedInterfaces();
				for (int i = 0; i < interfaces.size(); i++)
				{
					//set the return value
//					ParameterValueManagement.setQName(result, "Interface[" + i + "]/Type", interfaces.get(i).type );
					ParameterValueManagement.setString(result, "Interface[" + i + "]/LocalName", interfaces.get(i).localName);
					ParameterValueManagement.setString(result, "Interface[" + i + "]/Namespace", interfaces.get(i).namespace.toString());
					ParameterValueManagement.setString(result, "Interface[" + i + "]/Role", interfaces.get(i).role.name());		
//					ParameterValueManagement.setString(result, "Interface[" + i + "]/Role", Boolean.toString(interfaces.get(i).ruleSupport))	
					ParameterValueManagement.setAttributeValue(result, "Interface[" + i + "]", 
							new QName("ruleSupport", Constants.NAMESPACE_CONFIGURATION), Boolean.toString(interfaces.get(i).ruleSupport));
//					result.setAttributeValue(new QName("ruleSupport1", Constants.NAMESPACE), Boolean.toString(interfaces.get(i).ruleSupport));
				}						

				// ... and send it back
				return result;
			}

		});
	}
	
	private void configureConnectServiceOperation()
	{
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) getOperation(null, "ConnectService", null, null);

		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invokeImpl(Operation operation, ParameterValue request, CredentialInfo credentialInfo) throws InvocationException {
				System.out.println("Connect request: " + request);
				// extract the expected input args ...
				String address = ParameterValueManagement.getString(request, "Address");
				String devicePortTypeLocalName = ParameterValueManagement.getString(request, "Device/LocalName");
				String devicePortTypeNamespace = ParameterValueManagement.getString(request, "Device/Namespace");
				String portTypeLocalName = ParameterValueManagement.getString(request, "PortType/LocalName");
				String portTypeNamespace = ParameterValueManagement.getString(request, "PortType/Namespace");
				String serviceId = ParameterValueManagement.getString(request, "ServiceId");
				
				Endpoint endpoint = new Endpoint();
				endpoint.address = new URI(address);
				endpoint.device = new DPWSInterface();
				endpoint.device.localName = devicePortTypeLocalName;
				endpoint.device.namespace = new URI(devicePortTypeNamespace);
				if (portTypeLocalName != null && portTypeNamespace != null)
				{
					endpoint.portType = new DPWSInterface();
					endpoint.portType.localName = portTypeLocalName;
					endpoint.portType.namespace = new URI(portTypeNamespace);
				}
				if (serviceId != null)
				{
					endpoint.serviceId = new URI(serviceId);
				}
				
//				QName portType = new QName(portTypeLocalName, portTypeNamespace);
//				EndpointReference2004 endpointReference2004 = new EndpointReference2004(new URI(address), portType, null, null);
				
				client.connectService(endpoint);
				if (!isConnected(endpoint)) {
					int id = Math.abs(UUID.randomUUID().hashCode());
					connectedServices.put(id, endpoint);
				}

				// create suitable response ...
				ParameterValue result = operation.createOutputValue();
				
				return result;
			}

		});
	}
	
	private void configureGetRulesOperation()
	{
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) getOperation(null, "GetRules", null, null);

		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invokeImpl(Operation operation, ParameterValue request, CredentialInfo credentialInfo) throws InvocationException {
				// extract the expected input args ...
				//String value = ParameterValueManagement.getString(request, "someValue");

				// create suitable response ...
				ParameterValue result = operation.createOutputValue();
				
				int i = 0;
				for (Map.Entry<Integer, DPWSRule> entry: rulesList1.entrySet())
				{
					//set the return value
					for (int k = 0; k < entry.getValue().conditions.size(); k++) {
						ParameterValueManagement.setString(result, "Rule[" + i + "]/Conditions/Condition[" + k + "]/Parameter", entry.getValue().conditions.get(k).parameter);
						ParameterValueManagement.setString(result, "Rule[" + i + "]/Conditions/Condition[" + k + "]/Operator", entry.getValue().conditions.get(k).operator.name());
						try {
							ParameterValueManagement.setString(result, "Rule[" + i + "]/Conditions/Condition[" + k + "]/Value", entry.getValue().conditions.get(k).value.toString());
						} catch (Exception e) {
							ParameterValueManagement.setString(result, "Rule[" + i + "]/Conditions/Condition[" + k + "]/Value", "" + 0);
						}
					}
					ParameterValueManagement.setString(result, "Rule[" + i + "]/Connection", entry.getValue().connection.name());
					ParameterValueManagement.setString(result, "Rule[" + i + "]/Action/Operation", entry.getValue().action.operation);
					for (int j = 0; j < entry.getValue().action.parameters.size(); j ++) {
						ParameterValueManagement.setString(result, "Rule[" + i + "]/Action/Parameters/Parameter[" + j + "]/Name", entry.getValue().action.parameters.get(j).name);
						ParameterValueManagement.setString(result, "Rule[" + i + "]/Action/Parameters/Parameter[" + j + "]/Value", entry.getValue().action.parameters.get(j).value);
					}
					ParameterValueManagement.setAttributeValue(result, "Rule[" + i + "]", 
							new QName("name"), entry.getValue().name);
					ParameterValueManagement.setAttributeValue(result, "Rule[" + i + "]", 
							new QName("id"), "" + entry.getValue().id);
					i++;
				}						

				// ... and send it back
				return result;
			}

		});
	}
	
	private void configureGetRuleParametersOperation()
	{
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) getOperation(null, "GetRuleParameters", null, null);

		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invokeImpl(Operation operation, ParameterValue request, CredentialInfo credentialInfo) throws InvocationException {
				// extract the expected input args ...
				//String value = ParameterValueManagement.getString(request, "someValue");

				// create suitable response ...
				ParameterValue result = operation.createOutputValue();
				
				for (int i = 0; i < parameterDescriptionList.size(); i++)
				{
					//set the return value
					ParameterValueManagement.setString(result, "Parameter[" + i + "]/Name", parameterDescriptionList.get(i).name);
					ParameterValueManagement.setString(result, "Parameter[" + i + "]/Type", parameterDescriptionList.get(i).type.name());
				}						

				// ... and send it back
				return result;
			}

		});
	}
	
	private void configureSetRuleOperation()
	{
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) getOperation(null, "SetRule", null, null);

		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invokeImpl(Operation operation, ParameterValue request, CredentialInfo credentialInfo) throws InvocationException {
				// extract the expected input args ...
				//String value = ParameterValueManagement.getString(request, "someValue");
				System.out.println("SetRule: " + request);
				
				DPWSRule dpwsRule = new DPWSRule();
				for (int i = 0; i < request.getChildrenCount("Conditions/Condition"); i++) {	
					DPWSCondition condition = new DPWSCondition();
					condition.parameter = ParameterValueManagement.getString(request, "Conditions/Condition[" + i + "]/Parameter");
					condition.operator = Operator.valueOf(ParameterValueManagement.getString(request, "Conditions/Condition[" + i + "]/Operator"));
					condition.value = ParameterValueManagement.getString(request, "Conditions/Condition[" + i + "]/Value");
					dpwsRule.conditions.add(condition);
				}
				dpwsRule.connection = LogicOperation.valueOf(ParameterValueManagement.getString(request, "Connection"));
				dpwsRule.action.operation = ParameterValueManagement.getString(request, "Action/Operation");
				for (int i = 0; i < request.getChildrenCount("Action/Parameters/Parameter"); i++) {
					DPWSRuleParameter ruleParameter = new DPWSRuleParameter();
					ruleParameter.name = ParameterValueManagement.getString(request, "Action/Parameters/Parameter[" + i + "]/Name");
					ruleParameter.value = ParameterValueManagement.getString(request, "Action/Parameters/Parameter[" + i + "]/Value");
					dpwsRule.action.parameters.add(ruleParameter);
				}
				
				try {
					dpwsRule.id = Integer.parseInt(ParameterValueManagement.getAttributeValue(request, null, new QName("id")));
				} catch (Exception e) {
					dpwsRule.id = Math.abs(UUID.randomUUID().hashCode());
				}
				
				try {
					dpwsRule.name = ParameterValueManagement.getAttributeValue(request, null, new QName("name"));
				} catch (Exception e) {
					dpwsRule.name = null;
				}
				
				if (dpwsRule.name != null) {
					if (dpwsRule.name.isEmpty()) {
						dpwsRule.name = "Rule " + rulesList1.size();
					}
				} else {
					dpwsRule.name = "Rule " + rulesList1.size();
				}
					
				rulesList1.put(dpwsRule.id, dpwsRule);
				
				client.setRule(dpwsRule);

				// create suitable response ...
				ParameterValue result = operation.createOutputValue();
				
//				for (int i = 0; i < parameterDescriptionList.size(); i++)
//				{
//					//set the return value
//					ParameterValueManagement.setString(result, "Parameter[" + i + "]/Name", parameterDescriptionList.get(i).name);
//					ParameterValueManagement.setString(result, "Parameter[" + i + "]/Type", parameterDescriptionList.get(i).type.name());
//				}						

				// ... and send it back
				return result;
			}

		});
	}
	
	private void configureDeleteRuleOperation()
	{
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) getOperation(null, "DeleteRule", null, null);

		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invokeImpl(Operation operation, ParameterValue request, CredentialInfo credentialInfo) throws InvocationException {
				// extract the expected input args ...
				try {
					int index = Integer.parseInt(ParameterValueManagement.getString(request, null));
					DPWSRule dpwsRule = rulesList1.remove(index);
					if (dpwsRule != null) {
						System.out.println("Removed rule: " + index + " with name " + dpwsRule.name);
					}
				} catch (Exception e) {
					
				}
				
				// create suitable response ...
				ParameterValue result = operation.createOutputValue();					

				// ... and send it back
				return result;
			}

		});
	}
	
	private void configureGetConnectedServicesOperation()
	{
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) getOperation(null, "GetConnectedServices", null, null);

		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invokeImpl(Operation operation, ParameterValue request, CredentialInfo credentialInfo) throws InvocationException {
				// create suitable response ...
				ParameterValue result = operation.createOutputValue();

				int i = 0;
				for (Map.Entry<Integer, Endpoint> entry: connectedServices.entrySet()) {
					ParameterValueManagement.setString(result, "Service[" + i + "]/EndpointReference/Address", entry.getValue().address.toString());
					ParameterValueManagement.setString(result, "Service[" + i + "]/EndpointReference/Device/LocalName", entry.getValue().device.localName);
					ParameterValueManagement.setString(result, "Service[" + i + "]/EndpointReference/Device/Namespace", entry.getValue().device.namespace.toString());
					ParameterValueManagement.setString(result, "Service[" + i + "]/EndpointReference/PortType/LocalName", entry.getValue().portType.localName);
					ParameterValueManagement.setString(result, "Service[" + i + "]/EndpointReference/PortType/Namespace", entry.getValue().portType.namespace.toString());
					ParameterValueManagement.setString(result, "Service[" + i + "]/EndpointReference/ServiceId", entry.getValue().serviceId.toString());
					ParameterValueManagement.setAttributeValue(result, "Service[" + i + "]", new QName("id"), "" + entry.getKey());
					
					i++;
				}				

				// ... and send it back
				return result;
			}

		});
	}
	
	private void configureDisconnectServiceOperation()
	{
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) getOperation(null, "DisconnectService", null, null);

		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invokeImpl(Operation operation, ParameterValue request, CredentialInfo credentialInfo) throws InvocationException {
				ParameterValue result = null;
				
				try {
					int id = Integer.parseInt(ParameterValueManagement.getString(request, "Id"));
					Endpoint endpoint = connectedServices.remove(id);
					if (endpoint != null) {
						System.out.println("Removed device " + endpoint.device.localName);
					}
					rulesList1.clear();

					// create suitable response ...
					result = operation.createOutputValue();
				} catch(NumberFormatException e) {
					result = operation.createFaultValue("Wrong ID");
				}				

				// ... and send it back
				return result;
			}

		});
	}
	
	public void setParameter(DPWSParameterDescription description) {
		parameterDescriptionList.add(description);
	}
	
	private boolean isConnected(Endpoint endpoint) {
		for (Map.Entry<Integer, Endpoint> entry: connectedServices.entrySet()) {
			if (entry.getValue().address.equals(endpoint.address))
				return true;
		}
		return false;
	}

}
