/**
 * This project is a prototype that shows the idea of a DPWS "Connector" of a legacy system.
 * 
 * author:
 * Vlado Altmann
 * University of Rostock
 * Faculty of Computer Science and Electrical Engineering
 * Institute of Applied Microelectronics and Computer Engineering
 * 
 * The implementation uses code from the JMEDS Documentation and example code from Materna GmbH
 * See:
 * 		http://ws4d.e-technik.uni-rostock.de/jmeds/
 * and/or
 * 		http://sourceforge.net/projects/ws4d-javame/
 * 
 */

package com.pi.lightbulb.device;

import java.io.IOException;
import java.util.ArrayList;

import org.ws4d.java.CoreFramework;
import org.ws4d.java.communication.DPWSProtocolVersion;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.configuration.DispatchingProperties;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

import com.pi.lightbulb.configuration.ConfigurationClient;
import com.pi.lightbulb.configuration.ConfigurationService;
import com.pi.lightbulb.configuration.ConfigurationServiceInterface.DPWSInterface;
import com.pi.lightbulb.configuration.ConfigurationServiceInterface.DPWSParameterDescription;
import com.pi.lightbulb.configuration.ConfigurationServiceInterface.DataType;
import com.pi.lightbulb.configuration.ConfigurationServiceInterface.Role;
import com.pi.lightbulb.configuration.Constants;

public class LightBulbServiceProvider {

	/**
	 * @param args
	 */
	
	public static void main(String[] args) {
		
		// Disable debug output
		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
		DPWSProperties properties = DPWSProperties.getInstance();
//		properties.setDiscoveryButton0(1);		//einen von beiden aktivieren
//		properties.setDiscoveryButton1(3);
		properties.addSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2006);
//		properties.removeSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2009);
		DispatchingProperties.getInstance().setResponseWaitTime(10000);
		
		// mandatory: Starting the DPWS Framework.
		CoreFramework.start(args);
		
		// First we need a device.
		LightBulbDevice device = new LightBulbDevice();
		LightBulbService mainService = new LightBulbService();
		
				
		// Then we create a service.
		final ConfigurationService configurationService = new ConfigurationService();
		
		ConfigurationClient configurationClient = configurationService.getConfigurationClient();
		
		// Set supported interface types
		ArrayList<DPWSInterface> list = new ArrayList<DPWSInterface>();
		DPWSInterface dpwsInterface = new DPWSInterface();
//		dpwsInterface1.type = new QName("SensorInterface", Constants.NAMESPACE);
		dpwsInterface.localName = new String("Brightness");
		dpwsInterface.namespace = new URI(Constants.NAMESPACE);
		dpwsInterface.role = Role.Client;
		dpwsInterface.ruleSupport = true;
		list.add(dpwsInterface);
		
		configurationClient.setSupportedInterfaces(list);
		
		//Set rule parameter
		DPWSParameterDescription description = new DPWSParameterDescription();
		description.name = "Light";
		description.type = DataType.Integer;
		configurationService.setParameter(description);
		
		description = new DPWSParameterDescription();
		description.name = "Time";
		description.type = DataType.Time;
		configurationService.setParameter(description);

		// In the end we add our service to the device.
		device.addService(configurationService);
		device.addService(mainService);

		// Do not forget to start the device!
		try {
			device.start();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}
	

}
