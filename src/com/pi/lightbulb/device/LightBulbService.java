package com.pi.lightbulb.device;

import java.io.IOException;

import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.InvokeDelegate;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.OperationStub;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.URI;
import com.pi.lightbulb.configuration.Constants;

/** 
 * @author va015
 */
public class LightBulbService extends DefaultService {
	
	private boolean state = false;

	/**
	 * Standard Constructor
	 */
	public LightBulbService() {
		super(CommunicationManagerRegistry.getPreferredCommunicationManagerID());
		
		//Define Service ID 
//		setServiceId(new URI(Constants.NAMESPACE + "/" + Constants.DEVICE));
		setServiceId(new URI(Constants.SERVICE_ID));
		try {
			define(new URI(Constants.WSDL), CredentialInfo.EMPTY_CREDENTIAL_INFO, CommunicationManagerRegistry.getPreferredCommunicationManagerID());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		configureGetStateOperation();
		configureSetStateOperation();
		configureToggleOperation();
		configureswitchOnOperation();
		configureswitchOffOperation();

	}
	
	private void configureGetStateOperation()
	{
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) getOperation(null, "GetState", null, null);

		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invokeImpl(Operation operation, ParameterValue request, CredentialInfo credentialInfo) throws InvocationException {
				// extract the expected input args ...
				//String value = ParameterValueManagement.getString(request, "someValue");

				// create suitable response ...
				ParameterValue result = operation.createOutputValue();
				ParameterValueManagement.setString(result, null, Boolean.toString(state));

				// ... and send it back
				return result;
			}

		});
	}
	
	private void configureSetStateOperation()
	{
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) getOperation(null, "SetState", null, null);

		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invokeImpl(Operation operation, ParameterValue request, CredentialInfo credentialInfo) throws InvocationException {
				// extract the expected input args ...
				String value = ParameterValueManagement.getString(request, "State");

				state = Boolean.parseBoolean(value);
				
				if (state == true)
				{
					System.out.println("Light OFF");
				}
				else
				{
					System.out.println("Light OFF");
				}
				
				// create suitable response ...
				ParameterValue result = operation.createOutputValue();

				// ... and send it back
				return result;
			}

		});
	}
	
	private void configureToggleOperation()
	{
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) getOperation(null, "Toggle", null, null);

		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invokeImpl(Operation operation, ParameterValue request, CredentialInfo credentialInfo) throws InvocationException {
				// extract the expected input args ...
//				String value = ParameterValueManagement.getString(request, "State");
				
				state = !state;
				
				if (state == true)
				{
					System.out.println("Light ON");
				}
				else
				{
					System.out.println("Light OFF");
				}
				
				// create suitable response ...
				ParameterValue result = operation.createOutputValue();

				// ... and send it back
				return result;
			}

		});
	}
	
	private void configureswitchOnOperation()
	{
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) getOperation(null, "On", null, null);

		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invokeImpl(Operation operation, ParameterValue request, CredentialInfo credentialInfo) throws InvocationException {
				// extract the expected input args ...
//				String value = ParameterValueManagement.getString(request, "State");
				
				System.out.println("Light ON");

				// create suitable response ...
				ParameterValue result = operation.createOutputValue();

				// ... and send it back
				return result;
			}

		});
	}
	
	private void configureswitchOffOperation()
	{
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) getOperation(null, "Off", null, null);

		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invokeImpl(Operation operation, ParameterValue request, CredentialInfo credentialInfo) throws InvocationException {
				// extract the expected input args ...
//				String value = ParameterValueManagement.getString(request, "State");
				
				System.out.println("Light OFF");

				// create suitable response ...
				ParameterValue result = operation.createOutputValue();

				// ... and send it back
				return result;
			}

		});
	}

}
